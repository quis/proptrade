# Zasady i konwencje
- w opisie commit'a powinien znaleźć się identyfikator karty na Trello powiązanej z wykonanymi zmianami
- nazwy funkcji i zmiennych: camelCase
- nazwy pól w bazie danych: snake_case


# Projekt na Trello
Link do tablicy Trello: [klik](https://trello.com/b/0D4ZoBzd)  
Commity w opisie zawierają informacje o karcie Trello której dotyczą, identyfikator należy dokleić do adresu https://trello.com/c/, aby przejść do danej karty.