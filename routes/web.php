<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();


Route::get('/', 'DefaultController@index')->name('index');

Route::get('/panel', 'PanelController@index')->name('panel.index');

Route::get('/properties/{property}', 'PropertyController@show')->name('properties.show');

Route::get('/panel/properties/create', 'PropertyController@create')->name('properties.create');
Route::post('/panel/properties', 'PropertyController@store')->name('properties.store');
Route::get('/panel/properties/{property}/edit', 'PropertyController@edit')->name('properties.edit');
Route::put('/panel/properties/{property}', 'PropertyController@update')->name('properties.update');
Route::get('/panel/properties/{property}/close', 'PropertyController@close')->name('properties.close');
Route::get('/panel/properties/{property}/photos', 'PropertyController@photos')->name('properties.photos');
Route::post('/panel/properties/{property}/photos', 'PropertyController@photosUpdate')->name('properties.photos.update');
Route::get('/panel/user/change_password', 'PanelController@changePassword')->name('passwords.change');
Route::post('/panel/user/change_password', 'PanelController@updatePassword')->name('passwords.update');
Route::get('/panel/user/change_phone', 'PanelController@changePhone')->name('phone.change');
Route::post('/panel/user/change_phone', 'PanelController@updatePhone')->name('phone.update');

Route::post('/properties/{property}/messages', 'MessageController@store')->name('messages.store');
Route::get('/messages', 'MessageController@index')->name('messages.index');
Route::get('/messages/{message}/destroy', 'MessageController@destroy')->name('messages.destroy');
Route::get('/messages/{message}', 'MessageController@show')->name('messages.show');
