<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run() {
      DB::table('users')->insert([
         'name' => 'admin',
         'email' => 'admin@mail.pl',
         'password' => bcrypt('secret'),
         'phone' => rand(700000000, 900000000),
         'admin' => 1,
      ]);

      $names = [ 'Alojzy', 'Adam', 'Max', 'Daniel', 'Janusz' ];
      for ( $i = 0; $i < 5; $i++ ) {
         DB::table('users')->insert([
            'name' => $names[$i],
            'email' => $names[$i] . '@mail.pl',
            'password' => bcrypt($names[$i]),
            'phone' => rand(700000000, 900000000),
            'admin' => 0,
         ]);
      }
   }

}
