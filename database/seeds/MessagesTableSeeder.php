<?php

use Illuminate\Database\Seeder;

class MessagesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $now_dt = new DateTime();
        $before_5_days = new DateTime();
        $before_5_days->modify('-5 days');

        for ($i = 0; $i < 70; $i++) {
            $sender_id = DB::table('users')->inRandomOrder()->first()->id;
            $receiver_id = DB::table('users')->where('id', '!=', $sender_id)->inRandomOrder()->first()->id;
            $desc_len = 200;
            $word_len = 7;

            $description = '';
            while (strlen($description) < $desc_len)
                $description .= str_random($word_len) . ' ';
            $description = trim($description);

            $rand_dt = randomDate($before_5_days, $now_dt);
            $created_at = $rand_dt->format('Y-m-d H:i:s');
            
            DB::table('messages')->insert([
                'sender_id' => $sender_id,
                'receiver_id' => $receiver_id,
                'property_id' => DB::table('properties')->where('user_id', '=', $receiver_id)->inRandomOrder()->first()->id,
                'status' => rand(1, 2),
                'topic' => str_random(40),
                'content' => $description,
                'phone' => rand(700000000, 900000000),
                'created_at' => $created_at,
                'updated_at' => $created_at,
            ]);
        }
    }

}
