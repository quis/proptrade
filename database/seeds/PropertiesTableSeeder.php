<?php

use Illuminate\Database\Seeder;

class PropertiesTableSeeder extends Seeder {

   /**
    * Run the database seeds.
    *
    * @return void
    */
   public function run() {
      $prices = [ '100000.00', '200000.00', '150000.00', '250000.00', '300000.00', '400000.00' ];
      $locations = [ 'Poznań', 'Warszawa', 'Kraków', 'Gdynia', 'Sopot', 'Turek', 'Gdańsk', 'Zielona Góra' ];
      $photos = [ 'house1.jpg', 'house2.jpg', 'house3.jpg', 'house4.jpg' ];

      $now_dt = new DateTime();
      $before_14_days = new DateTime();
      $before_14_days->modify('-14 days');

      $desc_len = 1500;
      $word_len = 7;

      $description = '';
      while ( strlen($description) < $desc_len )
         $description .= str_random($word_len) . ' ';
      $description = trim($description);

      for ( $i = 0; $i < 50; $i++ ) {
         $rand_dt = randomDate($before_14_days, $now_dt);

         $id = DB::table('properties')->insertGetId([
            'name' => str_random(30),
            'price' => array_random($prices),
            'user_id' => DB::table('users')->inRandomOrder()->first()->id,
            'location' => array_random($locations),
            'yardage' => ( string ) rand(25, 80),
            'description' => $description,
            'closed' => rand(0, 1),
            'num_of_rooms' => rand(2, 20),
            'storey' => rand(0, 30),
            'balcony' => rand(0, 1),
            'created_at' => $rand_dt->format('Y-m-d H:i:s'),
            'updated_at' => $rand_dt->modify('+1 hour')->format('Y-m-d H:i:s'),
         ]);

         shuffle($photos);
         foreach ( $photos as $photo ) {
            DB::table('files')->insert([
               'name' => 'Random photo',
               'path' => 'public/images/uploaded/' . $photo,
               'owner_id' => $id,
               'owner_type' => 'App\Property',
            ]);
         }
      }
   }

}
