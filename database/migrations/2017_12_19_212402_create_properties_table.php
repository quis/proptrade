<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->string('name');
            $table->float('price', 10, 2);
            $table->float('yardage', 8, 2)->nullable();
            $table->smallInteger('num_of_rooms')->nullable();
            $table->smallInteger('storey')->nullable();
            $table->string('location')->nullable();
            $table->boolean('closed')->default('0');
            $table->boolean('balcony')->nullable();
            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('properties');
    }

}
