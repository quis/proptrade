<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Hasła muszą mieć co najmniej 6 znaków i do siebie pasować.',
    'reset' => 'Twoje hasło zostało zresetowane.',
    'sent' => 'Wysłaliśmy e-mail resetujący hasło na podany adres e-mail.',
    'token' => 'Twój token resetujący hasło jest nieprawidłowy.',
    'user' => "Nie znaleźliśmy użytkownika z podanym adresem e-mail.",

];
