@extends('layouts.app')

@section('content')
<h2>Zarządzanie kontem:</h2>
@if (session('after_password_change'))
<div class="alert alert-success">
    <p>Hasło zostało zmienione.</p>
</div>
@endif
@if (session('after_phone_change'))
<div class="alert alert-success">
    <p>Numer telefonu został zmieniony.</p>
</div>
@endif
<div class="list-group">
    <a href="{{ route('passwords.change') }}" role="button" class="list-group-item">zmień hasło</a>
    <a href="{{ route('phone.change') }}" role="button" class="list-group-item">zmień numer telefonu (@component('components.fields.phone', ['value' => $user->phone]) @endcomponent)</a>
</div>
<h2>Twoje ogłoszenia:</h2>
@if (session('after_property_close'))
<div class="alert alert-success">
    <p>Ogłoszenie {{ session('after_property_close') }} zostało zarchiwizowane.</p>
</div>
@endif
@component('property.components.list', ['properties' => $user->properties()->orderBy('created_at', 'desc')->paginate(10)])
@endcomponent
<a href="{{ route('properties.create') }}" class="btn btn-primary" role="button">Dodaj nowe ogłoszenie</a>
{{--@if ($user->admin)
<h2>Panel administracyjny:</h2>
@component('admin.components.panel') @endcomponent
@endif--}}
@endsection
