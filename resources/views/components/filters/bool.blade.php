<select class="form-group input-sm" name="{{ $name }}" id="{{ $name }}">
    <option value=""{{ ($value === '') ? ' selected' : '' }}></option>
    <option value="yes"{{ ($value === 'yes') ? ' selected' : '' }}>Tak</option>
    <option value="no"{{ ($value === 'no') ? ' selected' : '' }}>Nie</option>
</select>