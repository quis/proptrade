<div class="table-responsive">
<table class="table table-striped table-hover">
    <thead>
        <tr><th>Temat</th><th>Nadawca</th><th>Ogłoszenie</th><th>Numer telefonu</th><th>Data wysłania</th><th>Status</th></tr>
    </thead>
    <tbody>
        @forelse ($messages as $message)
        <tr>
            <td><a href="{{ route('messages.show', ['message' => $message->id]) }}">{{ $message->topic }}</a> @component('message.components.actions', [ 'message' => $message ]) @endcomponent</td>
            <td>{{ $message->sender->name }}</td>
            <td><a href="{{ route('properties.show', ['property' => $message->property->id]) }}">{{ $message->property->name }}</td>
            <td>@component('components.fields.phone', ['value' => $message->phone]) @endcomponent</td>
            <td>@component('components.fields.date-pretty', ['value' => $message->created_at]) @endcomponent</td>
            <td>@component('message.components.status', ['status' => $message->status]) @endcomponent</td>
        </tr>
        @empty
        <tr>
            <td colspan="5">Brak wiadomości</td>
        </tr>
        @endforelse
    </tbody>
</table>
</div>