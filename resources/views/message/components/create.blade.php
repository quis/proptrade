@if ($message_sent && !$errors->any())
<div class="alert alert-success">
    <p>Wiadomość została wysłana!</p>
</div>
@endif

<form action="{{ route('messages.store', ['property' => $property_id]) }}#send_message" method="POST" class="form-horizontal">
    {{ csrf_field() }}
    <div class="form-group{{ $errors->has('topic') ? ' has-error' : '' }}">
        <label for="topic" class="col-sm-2 control-label">Temat:</label>
        <div class="col-sm-10">
            <input type="text" name="topic" id="topic" class="form-control" value="{{ old('topic') }}" />
            @if ($errors->has('topic'))
            <span class="help-block">
                <strong>{{ $errors->first('topic') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('topic') ? ' has-error' : '' }}">
        <label for="phone" class="col-sm-2 control-label">Numer telefonu:</label>
        <div class="col-sm-10">
            <div class="input-group">
                <div class="input-group-addon">+48</div>
                <input type="number" name="phone" id="phone" class="form-control" value="{{ (!empty(old('phone'))) ? old('phone') : Auth::user()->phone }}" />
            </div>
            @if ($errors->has('phone'))
            <span class="help-block">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
        <label for="content" class="col-sm-2 control-label">Treść:</label>
        <div class="col-sm-10">
            <textarea name="content" id="content" class="form-control" rows="6">{{ old('content') }}</textarea>
            @if ($errors->has('content'))
            <span class="help-block">
                <strong>{{ $errors->first('content') }}</strong>
            </span>
            @endif
        </div>
    </div>
    <div class="col-sm-12">&nbsp;</div>
    <div class="col-sm-10 col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Wyślij wiadomość</button>
    </div>
</form>