<a href="{{ route('messages.destroy', ['message' => $message->id]) }}">
    <span class="glyphicon glyphicon-trash" aria-hidden="true" title="Usuń"></span>
    <span class="sr-only">Usuń</span>
</a>