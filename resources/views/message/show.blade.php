@extends('layouts.app')

@section('content')
@if ($message->receiver_id !== Auth::id())
<div class="row">
    <div class="col-md-12">
        <h2>Brak dostępu</h2>
        <a href="{{ route('index') }}"><span class="glyphicon glyphicon-backward"></span> Wróć na stronę główną</a>
    </div>
</div>
@else
<div class="row">
    <div class="col-md-3">
        <strong>Temat:</strong>
    </div>
    <div class="col-md-3">
        {{ $message->topic }}
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <strong>Ogłoszenie:</strong>
    </div>
    <div class="col-md-3">
        <a href="{{ route('properties.show', ['property' => $message->property->id]) }}">{{ $message->property->name }}</a>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <strong>Nadawca:</strong>
    </div>
    <div class="col-md-3">
        {{ $message->sender->name }}
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <strong>Numer telefonu nadawcy:</strong>
    </div>
    <div class="col-md-3">
        @component('components.fields.phone', ['value' => $message->phone]) @endcomponent
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <strong>Data odebrania wiadomości:</strong>
    </div>
    <div class="col-md-3">
        {{ $message->created_at }}
    </div>
    </div>
<div class="row">
    <div class="col-md-6">
        <strong>Treść:</strong>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <p>{{ $message->content }}</p>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <a href="{{ route('messages.index') }}" role="button" class="btn btn-default">Powrót do listy wiadomości</a>
    </div>
</div>
@endif
@endsection