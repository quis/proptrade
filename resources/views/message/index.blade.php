@extends('layouts.app')

@section('content')
<h2>Wiadomości:</h2>
@if (session('after_message_delete'))
<div class="alert alert-success">
    <p>Wiadomość została usunięta.</p>
</div>
@endif
@component('message.components.list', ['messages' => $messages]) @endcomponent
@endsection