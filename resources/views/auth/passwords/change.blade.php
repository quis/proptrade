@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Zmień hasło</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('passwords.update') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Aktualne hasło</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_new') ? ' has-error' : '' }}">
                            <label for="password-new" class="col-md-4 control-label">Nowe hasło</label>
                            <div class="col-md-6">
                                <input id="password-new" type="password" class="form-control" name="password_new" required>

                                @if ($errors->has('password_new'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_new') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password_new_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Powtórz hasło</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_new_confirmation" required>

                                @if ($errors->has('password_new_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_new_confirmation') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Zmień hasło
                                </button>
                                <a href="{{ route('panel.index') }}" role="button" class="btn btn-default">
                                    Anuluj
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
