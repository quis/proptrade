<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-default navbar-static-top main-navbar">
                <div class="container">
                    <div class="navbar-header">

                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                            <span class="sr-only">Przełącz nawigację</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a href="{{ url('/') }}">
                            <img src="{{ asset('images/logo.png') }}" class="logo" alt="PropTrade logo" title="PropTrade" />
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="app-navbar-collapse">
                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            @guest
                            <li><a href="{{ route('login') }}">Zaloguj</a></li>
                            <li><a href="{{ route('register') }}">Zarejestruj</a></li>
                            @else
                            <li><a href="{{ route('messages.index') }}">Wiadomości <span class="badge">{{ Auth::user()->unreadedMessagesCount() }}</span></a></li>
                            <li><a href="{{ route('panel.index') }}">Panel</a></li>
                            <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById( 'logout-form' ).submit();">Wyloguj ({{ Auth::user()->name }})</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>  
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>

            @yield('before-content')
            <div class="container main-container">
                @yield('content')
            </div>

            <footer>
                <p class="text-center footer">Copyright &copy; 2017<br />Dawid Zaroda &amp; Michał Śramski</p>
            </footer>
        </div>
    </body>
</html>
