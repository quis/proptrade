@extends('layouts.app')

@section('before-content')
@guest
<div class="container">
    <div class="jumbotron">
        <h1>PropTrade</h1>
        <p>Witamy Cię na naszej platformie wymiany nieruchomościami! Poniżej znajdują się najnowsze ogłoszenia. Jeśli chciałbyś dodać nowe ogłoszenie, utwórz konto, korzystając z przycisku poniżej.</p>
        <div class="btn-group" role="group">
            <a class="btn btn-primary btn-lg" href="{{ route('register') }}" role="button">Rejestracja</a>
            <a class="btn btn-default btn-lg" href="#" role="button">O nas</a>
        </div>
    </div>
</div>
@else
<div class="container">
    <div class="jumbotron">
        <h1>PropTrade</h1>
        <p>Chcesz sprzedać nieruchomość? Wystaw ją w naszym serwisie!<br />Wystarczy, że skorzystasz z przycisku poniżej.</p>
        <a class="btn btn-primary btn-lg" href="{{ route('properties.create') }}" role="button">Dodaj ogłoszenie</a>
    </div>
</div>
@endguest
@endsection

@section('content')
@component('property.components.filters', ['oldFiltersData' => $oldFiltersData]) @endcomponent
@component('property.components.list', ['properties' => $properties]) @endcomponent

@if (count($recentlyProperties) > 0)
<h2>Ostatnio oglądane ogłoszenia:</h2>
@component('property.components.list', ['properties' => $recentlyProperties])
@endcomponent
@endif
@endsection
