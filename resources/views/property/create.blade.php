@extends('layouts.app')

@section('content')
<h1>Dodaj nowe ogłoszenie</h1>
@component('property.components.edit', ['route' => route('properties.store'), 'allow_photos' => true]) @endcomponent
@endsection