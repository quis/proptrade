@extends('layouts.app')

@section('content')
@if ($user->id !== Auth::id() && $property->closed)
<div class="row">
    <div class="col-md-12">
        <h2>Brak dostępu</h2>
        <a href="{{ route('index') }}"><span class="glyphicon glyphicon-backward"></span> Wróć na stronę główną</a>
    </div>
</div>
@else
<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <h2>{{ $property->name }}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <strong>Cena:</strong>
            </div>
            <div class="col-md-6">
                @component('components.fields.currency', ['value' => $property->price]) @endcomponent
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <strong>Lokalizacja:</strong>
            </div>
            <div class="col-md-6">
                @empty($property->location)
                    Brak danych
                @else
                    {{ $property->location }}
                 @endempty
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <strong>Metraż:</strong>
            </div>
            <div class="col-md-6">
                @empty($property->yardage)
                    Brak danych
                @else
                    @component('components.fields.yardage', ['value' => $property->yardage]) @endcomponent
                 @endempty
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <strong>Liczba pokoi:</strong>
            </div>
            <div class="col-md-6">
                @empty($property->num_of_rooms)
                    Brak danych
                @else
                    {{$property->num_of_rooms }}
                 @endempty
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <strong>Balkon:</strong>
            </div>
            <div class="col-md-6">
                @component('components.fields.bool', ['value' => $property->balcony]) @endcomponent
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <strong>Kondygnacja:</strong>
            </div>
            <div class="col-md-6">
                @empty($property->storey)
                    Brak danych
                @else
                    {{ $property->storey }}
                @endempty
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <strong>Wystawione przez:</strong>
            </div>
            <div class="col-md-6">
                {{ $user->name }}
                @if ($user->id === Auth::id())
                (Ty)
                @endif
            </div>
        </div>
        
        @if ($user->id === Auth::id() && !$property->closed)
        <div class="row">
            <div class="col-md-12">
                &nbsp;
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <strong>Opcje:</strong>
            </div>
            <div class="col-md-6">
               @component('property.components.actions', ['property_id' => $property->id]) @endcomponent
            </div>
        </div>
        @endif
    </div>
    @isset($property->files[0])
        <div class="col-md-6">
            <img src="{{ asset($property->files[0]->getPublicPath()) }}" class="img-responsive img-thumbnail property-main-img" />
        </div>
    @endisset
</div>
@if (!empty($property->description))
<div class="row">
    <div class="col-md-12">
        <h3>Opis:</h3>
        <p>{{ $property->description }}</p>
    </div>
</div>
@endif
@auth
@if ($user->id !== Auth::id())
<div class="row" id="send_message">
    <div class="col-md-12">
        <h3>Wyślij wiadomość</h3>
        <p>Wyślij prywatną wiadomość do sprzedającego, co pozwoli mu udzielić odpowiedzi na wszelkie Twoje pytania.</p>
        @component('message.components.create', ['property_id' => $property->id, 'message_sent' => isset($message_sent) ? $message_sent : false]) @endcomponent
    </div>
</div>
@endif
@endauth
@if(count($property->files) > 1)
<div class="row">
    <div class="col-md-12">
        <h3>Galeria:</h3>
        <div class="row">
        @foreach($property->files as $file)
            <div class="col-md-6">
                <img src="{{ asset($file->getPublicPath()) }}" class="img-responsive img-thumbnail" />
            </div>
        @endforeach
        </div>
    </div>
</div>
@endif
@endif
@endsection