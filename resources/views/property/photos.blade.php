@extends('layouts.app')

@section('content')
<h1>Zmień zdjęcia ogłoszenia</h1>
<div class="alert alert-warning" role="alert"><strong>Ostrzeżenie!</strong> Jeśli wgrasz nowe zdjęcia, stare zostaną usunięte.</div>
<form action="{{ route('properties.photos.update', ['property' => $property->id]) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    @component('property.components.photos') @endcomponent
    <div class="col-sm-12">&nbsp;</div>
    <div class="col-sm-10 col-sm-offset-2">
        <a role="button" class="btn btn-default" href="{{ route('panel.index') }}">Anuluj</a>
        <button type="submit" class="btn btn-primary">Dodaj zdjęcia</button>
    </div>
</form>
@endsection