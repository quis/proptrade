<a href="{{ route('properties.edit', ['property' => $property_id] ) }}">
    <span class="glyphicon glyphicon-edit" aria-hidden="true" title="Edytuj"></span>
    <span class="sr-only">Edytuj</span>
</a>
<a href="{{ route('properties.photos', ['property' => $property_id] ) }}">
    <span class="glyphicon glyphicon-file" aria-hidden="true" title="Edytuj zdjęcia"></span>
    <span class="sr-only">Wgraj nowe zdjęcia</span>
</a>
<a href="{{ route('properties.close', ['property' => $property_id] ) }}">
    <span class="glyphicon glyphicon-remove" aria-hidden="true" title="Zakończ"></span>
    <span class="sr-only">Zakończ</span>
</a>