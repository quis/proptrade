@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<form action="{{ $route }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if (isset($method))
    {{ method_field($method) }}
    @endif
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Nazwa:</label>
        <div class="col-sm-10">
            <input type="text" name="name" id="name" class="form-control" value="{{ isset($property) ? $property->name : old('name') }}" />
        </div>
    </div>
    <div class="form-group">
        <label for="price" class="col-sm-2 control-label">Cena:</label>
        <div class="col-sm-10">
            <div class="input-group">
                <input type="number" name="price" id="price" class="form-control" value="{{ isset($property) ? $property->price : old('price') }}" />
                <div class="input-group-addon">zł</div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="yardage" class="col-sm-2 control-label">Metraż:</label>
        <div class="col-sm-10">
            <div class="input-group">
                <input type="text" name="yardage" id="yardage" class="form-control" value="{{ isset($property) ? $property->yardage : old('yardage') }}" />
                <div class="input-group-addon">m</div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="num_of_rooms" class="col-sm-2 control-label">Liczba pokoi:</label>
        <div class="col-sm-10">
            <input type="number" name="num_of_rooms" id="num_of_rooms" class="form-control" value="{{ isset($property) ? $property->num_of_rooms : old('num_of_rooms') }}" min="1" max="50"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-2 control-label">
            <label>Balkon:</label>
        </div>
        <div class="col-sm-10">
            <input type="checkbox" name="balcony" id="balcony" value="1" {{ isset($property) ? (($property->balcony == 1) ? 'checked ' : '') : ((old('balcony') == 1) ? 'checked ' : '') }}/>
        </div>
    </div>
    <div class="form-group">
        <label for="storey" class="col-sm-2 control-label">Kondygnacja:</label>
        <div class="col-sm-10">
            <input type="number" name="storey" id="storey" class="form-control" value="{{ isset($property) ? $property->storey : old('storey') }}" min="0" max="50" />
        </div>
    </div>
    <div class="form-group">
        <label for="location" class="col-sm-2 control-label">Lokalizacja:</label>
        <div class="col-sm-10">
            <input type="text" name="location" id="location" class="form-control" value="{{ isset($property) ? $property->location : old('location') }}" />
        </div>
    </div>
    <div class="form-group">
        <label for="description" class="col-sm-2 control-label">Opis:</label>
        <div class="col-sm-10">
            <textarea name="description" id="description" rows="6" class="form-control">{{ isset($property) ? $property->description : old('description') }}</textarea>
        </div>
    </div>
    @if ($allow_photos)
    @component('property.components.photos') @endcomponent
    @endif
    <div class="col-sm-12">&nbsp;</div>
    <div class="col-sm-10 col-sm-offset-2">
        <a role="button" class="btn btn-default" href="{{ route('panel.index') }}">Anuluj</a>
        <button type="submit" class="btn btn-primary">Zapisz</button>
    </div>
</form>