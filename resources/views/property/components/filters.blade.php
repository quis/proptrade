<h2>Wyszukiwarka</h2>
<form action="{{ route('index') }}" method="GET" class="form-inline">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-md-2 text-nowrap">
            <label>Liczba pokoi:</label>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="num_of_rooms_from">od</label>
                <input type="number" name="num_of_rooms_from" class="form-control input-sm" id="num_of_rooms_from" min="1" value="{{ !empty($oldFiltersData['num_of_rooms_from']) ? $oldFiltersData['num_of_rooms_from'] : ''}}">
            </div>
            <div class="form-group">
                <label for="num_of_rooms_to">do</label>
                <input type="number" name="num_of_rooms_to" class="form-control input-sm" id="num_of_rooms_to" max="50" value="{{ !empty($oldFiltersData['num_of_rooms_to']) ? $oldFiltersData['num_of_rooms_to'] : ''}}">
            </div>
        </div>
        <div class="col-md-2 text-nowrap">
            <label>Powierzchnia mieszkania:</label>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="yardage_from">od</label>
                <div class="input-group">
                    <input type="number" name="yardage_from" class="form-control input-sm" id="yardage_from" min="1" value="{{ !empty($oldFiltersData['yardage_from']) ? $oldFiltersData['yardage_from'] : ''}}">
                    <div class="input-group-addon">m</div>
                </div>
            </div>
            <div class="form-group">
                <label for="yardage_to">do</label>
                <div class="input-group">
                    <input type="number" name="yardage_to" class="form-control input-sm" id="yardage_to" value="{{ !empty($oldFiltersData['yardage_to']) ? $oldFiltersData['yardage_to'] : ''}}">
                    <div class="input-group-addon">m</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 text-nowrap">
            <label>Cena:</label>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="price_from">od</label>
                <div class="input-group">
                    <input type="number" name="price_from" class="form-control input-sm" id="price_from" min="1" value="{{ !empty($oldFiltersData['price_from']) ? $oldFiltersData['price_from'] : ''}}">
                    <div class="input-group-addon">zł</div>
                </div>
            </div>
            <div class="form-group">
                <label for="price_to">do</label>
                <div class="input-group">
                    <input type="number" name="price_to" class="form-control input-sm" id="price_to" value="{{ !empty($oldFiltersData['price_to']) ? $oldFiltersData['price_to'] : ''}}">
                    <div class="input-group-addon">zł</div>
                </div>
            </div>
        </div>
        <div class="col-md-2 text-nowrap">
            <label for="storey">Kondygnacja:</label>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <input type="number" name="storey" class="form-control input-sm" id="storey" min="0" max="50" value="{{ !empty($oldFiltersData['storey']) ? $oldFiltersData['storey'] : ''}}">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 text-nowrap">
            <label for="balcony">Balkon:</label>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                @component('components.filters.bool', ['name' => 'balcony', 'value' => isset($oldFiltersData['balcony']) ? $oldFiltersData['balcony'] : '']) @endcomponent
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <input type="submit" class="btn btn-primary" value="Filtruj" />
            <a href="{{ route('index') }}" role="button" class="btn btn-default">Wyczyść</a>
        </div>
    </div>
</form>