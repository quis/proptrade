<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead>
            <tr><th>Nieruchomość</th><th>Cena</th><th>Lokalizacja</th><th>Powierzchnia</th><th>Dodano</th></tr>
        </thead>
        <tbody>
            @forelse ($properties as $property)
            @if ($property->closed == 0)
            <tr>
                @else
            <tr class="warning">
                @endif
                <td>
                    <a href="{{ route('properties.show', ['property' => $property->id]) }}">{{ $property->name }}</a>
                    @if($property->user_id === Auth::id())
                    @if($property->closed)
                    <small>(archiwum)</small>
                    @else
                    @component('property.components.actions', ['property_id' => $property->id]) @endcomponent
                    @endif
                    @endif
                </td>
                <td>@component('components.fields.currency', ['value' => $property->price]) @endcomponent</td>
                <td>{{ $property->location }}</td>
                <td>@component('components.fields.yardage', ['value' => $property->yardage]) @endcomponent</td>
                <td>@component('components.fields.date-pretty', ['value' => $property->created_at]) @endcomponent</td>
            </tr>
            @empty
            <tr>
                <td colspan="5">Brak ogłoszeń</td>
            </tr>
            @endforelse
        </tbody>
        @if (method_exists($properties, 'links'))
        <tfoot>
            <tr>
                <td colspan="5" class="center">
                    <nav aria-label="Nawigacja stron">
                        <ul class="pagination">
                            {{ $properties->links() }}
                        </ul>
                    </nav>
                </td>
            </tr>
        </tfoot>
        @endif
    </table>
</div>