<div class="form-group">
    <label for="photo[0]" class="col-sm-2 control-label">Zdjęcia:</label>
    <div class="col-sm-10">
        <input type="file" name="photo[0]" id="photo[0]" value="{{ old('photo[0]') }}" />
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="file" name="photo[1]" id="photo[1]" value="{{ old('photo[1]') }}" />
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="file" name="photo[2]" id="photo[2]" value="{{ old('photo[2]') }}" />
    </div>
</div> 
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="file" name="photo[3]" id="photo[3]" value="{{ old('photo[3]') }}" />
        <p class="help-block">Obsługiwane formaty: jpg, png. Maksymalny rozmiar pliku: 1 MB</p>
    </div>
</div>
{{--<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <button type="button" class="btn btn-primary btn-sm">Dodaj kolejne zdjęcie</button>
    </div>
</div>--}}
