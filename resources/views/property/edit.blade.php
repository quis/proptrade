@extends('layouts.app')

@section('content')
<h1>Edytuj ogłoszenie</h1>
@component('property.components.edit', ['route' => route('properties.update', ['property' => $property->id]), 'method' => 'PUT', 'allow_photos' => false, 'property' => $property]) @endcomponent
@endsection