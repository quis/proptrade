<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;

class DefaultController extends Controller {

   public function index(Request $request) {
      $validated_data = $request->validate([
         'price_from' => 'nullable|numeric|min:1|digits_between:1,8',
         'price_to' => 'nullable|numeric|min:1|digits_between:1,8',
         'yardage_from' => 'nullable|min:1|digits_between:1,6',
         'yardage_to' => 'nullable|min:1|digits_between:1,6',
         'num_of_rooms_from' => 'nullable|integer|between:1,50',
         'num_of_rooms_to' => 'nullable|integer|between:1,50',
         'storey' => 'nullable|integer|between:0,50',
      ]);

      $recentlyPropertiesCookie = $request->cookie('recent_properties');
      $recentlyProperties = isset($recentlyPropertiesCookie) ? $recentlyPropertiesCookie : [];

      $properties = Property::orderBy('created_at', 'desc')->where('closed', 0);
      $filterData = [];
      $filters = [
         'num_of_rooms_from' => [
            'field' => 'num_of_rooms',
            'operator' => '>=',
         ],
         'num_of_rooms_to' => [
            'field' => 'num_of_rooms',
            'operator' => '<=',
         ],
         'yardage_from' => [
            'field' => 'yardage',
            'operator' => '>=',
         ],
         'yardage_to' => [
            'field' => 'yardage',
            'operator' => '<=',
         ],
         'price_from' => [
            'field' => 'price',
            'operator' => '>=',
         ],
         'price_to' => [
            'field' => 'price',
            'operator' => '<=',
         ],
         'storey' => [
            'field' => 'storey',
            'operator' => '=',
         ],
         'balcony' => [
            'field' => 'balcony',
            'operator' => '=',
            'type' => 'bool',
         ],
      ];
      foreach ( $filters as $filter => $meta ) {
         if ( !empty($request->$filter) ) {
            $value = $request->$filter;
            if ( isset($meta['type']) && $meta['type'] === 'bool' ) {
               $value = $value === 'yes' ? 1 : 0;
            }
            $properties->where($meta['field'], $meta['operator'], $value);

            $filterData[$filter] = $request->$filter;
         }
      }

      return view('index', [
         'oldFiltersData' => $filterData,
         'properties' => $properties->paginate(20),
         'recentlyProperties' => Property::whereIn('id', $recentlyProperties)->orderBy('created_at', 'desc')->get()
      ]);
   }

}
