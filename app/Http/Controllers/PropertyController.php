<?php

namespace App\Http\Controllers;

use App\User;
use App\Property;
use App\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class PropertyController extends Controller {

   public function create() {
      return view('property.create');
   }

   public function store(Request $request) {
      $validated_data = $request->validate([
         'name' => 'required',
         'price' => 'required|numeric|min:1|digits_between:1,8',
         'yardage' => 'digits_between:0,6',
         'num_of_rooms' => 'nullable|integer|between:1,50',
         'storey' => 'nullable|integer|between:0,50',
         'photo.*' => 'mimes:jpeg,jpg,png|max:1024',
      ]);

      $property = new Property;

      $property->fill($request->all());
      $property->user_id = Auth::id();

      $property->save();

      $photo_count = 4;
      for ( $idx = 0; $idx < $photo_count; $idx++ ) {
         $input_name = 'photo.' . $idx;

         if ( $request->hasFile($input_name) ) {
            $property->saveFile($request->file($input_name));
         }
      }

      return redirect()->route('properties.show', [ 'id' => $property->id ]);
   }

   public function show(Request $request, Property $property) {
      if ( !$property->closed ) {
         $recentlyPropertiesCookie = $request->cookie('recent_properties');
         $recentlyProperties = isset($recentlyPropertiesCookie) ? $recentlyPropertiesCookie : [];

         if ( !in_array($property->id, $recentlyProperties) )
            $recentlyProperties[] = $property->id;

         if ( count($recentlyProperties) > 5 )
            array_shift($recentlyProperties);

         Cookie::queue('recent_properties', $recentlyProperties, '30');
      }
      return view('property.show', [ 'property' => $property, 'user' => User::find($property->user_id) ]);
   }

   public function edit(Property $property) {
      return view('property.edit', [ 'property' => $property ]);
   }

   public function photos(Property $property) {
      return view('property.photos', [ 'property' => $property ]);
   }

   public function update(Request $request, Property $property) {
      $validated_data = $request->validate([
         'name' => 'required',
         'price' => 'required|numeric|min:1|digits_between:1,8',
         'yardage' => 'digits_between:0,6',
         'num_of_rooms' => 'nullable|integer|between:1,50',
         'storey' => 'nullable|integer|between:0,50',
      ]);

      $property->fill($request->all());
      $property->save();
      return redirect()->route('properties.show', [ 'id' => $property->id ]);
   }

   public function photosUpdate(Request $request, Property $property) {
      $validated_data = $request->validate([
         'photo.*' => 'mimes:jpeg,jpg,png|max:1024',
      ]);

      $photo_count = 4;
      $files_cleared = false;
      for ( $idx = 0; $idx < $photo_count; $idx++ ) {
         $input_name = 'photo.' . $idx;

         if ( $request->hasFile($input_name) ) {
            if ( !$files_cleared ) {
               $property->clearFiles();
               $files_cleared = true;
            }
            $property->saveFile($request->file($input_name));
         }
      }
      return redirect()->route('properties.show', [ 'id' => $property->id ]);
   }

   public function close(Property $property) {
      if ( $property->user_id === Auth::id() ) {
         $property->closed = true;
         $property->save();
         return redirect()->route('panel.index')->with('after_property_close', $property->name);
      }
      return view('index');
   }

}
