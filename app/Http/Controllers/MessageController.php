<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Property;
use App\Message;

class MessageController extends Controller {

    public function store(Request $request, Property $property) {
        $validated_data = $request->validate([
            'topic' => 'required|string|min:5',
            'content' => 'required|string|min:5',
            'phone' => 'required|numeric|digits_between:9,15',
        ]);

        $message = new Message;
        $message->topic = $request->topic;
        $message->content = $request->content;
        $message->status = Message::STATUS_SENT;
        $message->phone = $request->phone;

        $message->property_id = $property->id;
        $message->receiver_id = $property->user_id;
        $message->sender_id = Auth::id();

        $message->save();


        return view('property.show', ['property' => $property, 'user' => User::find($property->user_id), 'message_sent' => true]);
    }

    public function index() {
        $messages = Auth::user()->receivedMessages()->orderBy('status')->orderBy('created_at', 'desc')->get();
        return view('message.index', ['messages' => $messages]);
    }

    public function show(Message $message) {
        if (Auth::id() === $message->receiver_id && $message->status === Message::STATUS_SENT) {
            $message->status = Message::STATUS_READ;
            $message->save();
        }
        return view('message.show', ['message' => $message]);
    }

    public function destroy(Request $request, Message $message) {
        $message->delete();
        return Redirect::route('messages.index')->with('after_message_delete', true);
    }

}
