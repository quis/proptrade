<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\MessageBag;
use Validator;

class PanelController extends Controller {

   public function __construct() {
      $this->middleware('auth');
   }

   public function index() {
      return view('panel.index', [ 'user' => \Auth::user() ]);
   }

   public function changePassword() {
      return view('auth.passwords.change');
   }

   public function updatePassword(Request $request) {
      $validator = Validator::make($request->all(), [
                 'password' => 'required',
                 'password_new' => 'required|min:5|confirmed',
                 'password_new_confirmation' => 'required|min:5',
      ]);
      $errors = $validator->getMessageBag();
      $error = false;
      $user = Auth::user();

      $valid = Auth::guard()->attempt([ 'name' => $user->name, 'password' => $request->password ]);
      if ( !$valid ) {
         $errors->add('password', 'Hasło jest nieprawidłowe.');
         $error = true;
      }

      if ( !$validator->fails() && !$error ) {

         $user->password = bcrypt($request->password_new);
         $user->save();
         return redirect()->route('panel.index')->with('after_password_change', true);
      }
      return view('auth.passwords.change', [ 'errors' => $errors ]);
   }

   public function changePhone() {
      return view('auth.phone.change');
   }

   public function updatePhone(Request $request) {
      $validator = Validator::make($request->all(), [
                 'password' => 'required',
                 'phone' => 'required|numeric|digits_between:9,15',
      ]);
      $errors = $validator->getMessageBag();
      $error = false;
      $user = Auth::user();
      $valid = Auth::guard()->attempt([ 'name' => $user->name, 'password' => $request->password ]);

      if ( !$valid ) {
         $errors->add('password', 'Hasło jest nieprawidłowe.');
         $error = true;
      }

      if ( !$validator->fails() && !$error ) {
         $user->phone = $request->phone;
         $user->save();
         return redirect()->route('panel.index')->with('after_phone_change', true);
      }
      return view('auth.phone.change', [ 'errors' => $errors ]);
   }

}
