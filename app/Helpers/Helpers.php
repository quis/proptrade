<?php

function randomDate(DateTime $start, DateTime $end) {
   $start_ts = $start->getTimestamp();
   $end_ts = $end->getTimestamp();
   $rand_dt = new DateTime();
   return ($rand_dt->setTimestamp(rand($start_ts, $end_ts)));
}
