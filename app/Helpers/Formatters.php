<?php

const WEEK_DAYS = ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'];

function prettyFormatDate($date) {
    $datetime = new DateTime($date);
    if (!$datetime) {
        return $date;
    } else {
        $diff = $datetime->diff(new DateTime());
        if ($diff->d < 0) {
            return 'W przyszłości';
        }

        if ($diff->d > 6) {
            return $datetime->format('d/m/Y');
        }
        switch ($diff->d) {
            case 0:
                return 'Dzisiaj';
            case 1:
                return 'Wczoraj';
            default:
                $weekday = intval($datetime->format('w'));
                return WEEK_DAYS[$weekday];
        }
    }
}
