<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Property extends Model {

   protected $fillable = [
      'name',
      'price',
      'yardage',
      'num_of_rooms',
      'storey',
      'location',
      'description',
      'user_id',
      'balcony',
   ];

   public function user() {
      return $this->belongsTo('App\User');
   }

   public function files() {
      return $this->morphMany('App\File', 'owner');
   }

   public function messages() {
      return $this->hasMany('App\Message');
   }

   public function saveFile($input_file) {
      $path = $input_file->storePublicly('public/images/uploaded');

      $file = new File;
      $file->name = $input_file->getClientOriginalName();
      $file->path = $path;
      $file->owner_id = $this->id;
      $file->owner_type = 'App\Property';
      $file->save();
   }

   public function clearFiles() {
      foreach ( $this->files as $file ) {
         $contents = Storage::delete($file->path);
         $file->delete();
      }
   }

}
