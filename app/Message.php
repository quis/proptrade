<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    const STATUS_SENT = 1;
    const STATUS_READ = 2;

    public function sender() {
        return $this->belongsTo('App\User', 'sender_id');
    }

    public function receiver() {
        return $this->belongsTo('App\User', 'receiver_id');
    }

    public function property() {
        return $this->belongsTo('App\Property');
    }

}
