<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Message;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function properties() {
        return $this->hasMany('App\Property');
    }

    public function sentMessages() {
        return $this->hasMany('App\Message', 'sender_id');
    }

    public function receivedMessages() {
        return $this->hasMany('App\Message', 'receiver_id');
    }

    public function unreadedMessagesCount() {
        return $this->receivedMessages()->where('status', '=', Message::STATUS_SENT)->count();
    }

}
