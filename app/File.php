<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model {

    public function owner() {
        return $this->morphTo();
    }

    public function getPublicPath() {
        $path_array = explode('/', $this->path);
        if ($path_array[0] === 'public') {
            $path_array[0] = 'storage';
        }
        return implode('/', $path_array);
    }

}
